---
author:
- Shockrah
title:
- exp-make
subtitle:
- Pandoc script to easily write markdown docs and use it with exp
section:
- 1
header:
- exmmp Makes Man Pages
footer:
- <3 Shockrah
---

# Description

The exp script-suite is a set of scripts _listed below_ to easily create documentation in markdown and package it for developers.

## But why?

Because there's tons of man pages that desperately lack example so this tool should help people write _something_ when man pages fail the rest

# Usage

## Input

```
exp-make doc1.md doc2.markdown doc3.md doc4 ...
```

## Output 

```
doc1.groff doc2.groff doc3.groff
```


# Suite tools

- exp-make files [...]

each file by default is written to the equivalent `$(basename).groff` file in the current working directory.

- exp-install files [..]
